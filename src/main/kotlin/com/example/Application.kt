package com.example

import com.example.data.FileStorage
import com.example.data.MemoryStorage
import com.example.data.Storage
import com.example.data.UserSession
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.example.plugins.*
import io.ktor.http.*
import io.ktor.server.auth.Authentication
import io.ktor.server.auth.*
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import io.ktor.util.*

fun main() {
    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    val storageType = "file" // Change to "file" for file storage
    val storage: Storage = when (storageType) {
        "memory" -> MemoryStorage()
        "file" -> FileStorage("countries.txt")
        else -> throw IllegalArgumentException("Invalid storage type")
    }
    attributes.put(AttributeKey("storage"), storage)

    install(Sessions) {
        cookie<UserSession>("user_session") {
            cookie.path = "/"
            cookie.maxAgeInSeconds = 3600
        }
    }

    install(Authentication) {
        session<UserSession>("auth-session") {
            validate { session ->
                if(session.name.startsWith("Eduard")) {
                    session
                } else {
                    null
                }
            }
            challenge {
                call.respondRedirect("/")
            }
        }

        form("auth-form") {
            userParamName = "username"
            passwordParamName = "password"
            validate { credentials ->
                if (credentials.name == "Eduard" && credentials.password == "Eduard123") {
                    UserIdPrincipal(credentials.name)
                } else {
                    null
                }
            }
            challenge {
                call.respond(HttpStatusCode.Unauthorized, "Credentials are not valid")
            }
        }
    }
    configureRouting()
}
