package com.example.data

import kotlinx.serialization.Serializable

@Serializable
data class Country(
    val name: CountryName,
    val flags: CountryFlags
)

@Serializable
data class CountryName(
    val common: String,
    val official: String,
    val nativeName: Map<String, CountryNativeName>
)

@Serializable
data class CountryNativeName(
    val official: String,
    val common: String
)

@Serializable
data class CountryFlags(
    val png: String,
    val svg: String,
    val alt: String
)
