package com.example.data

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File

interface Storage {
    fun addCountry(country: String)
    fun getCountries(): List<String>
}

class MemoryStorage : Storage {
    private val countries = mutableListOf<String>()

    override fun addCountry(country: String) {
        countries.add(country)
    }

    override fun getCountries(): List<String> {
        return countries
    }
}

class FileStorage(private val filename: String) : Storage {
    private val countries = mutableListOf<String>()

    init {
        val json = File(filename).readText()
        val countriesData = Json.decodeFromString<List<Country>>(json)
        countries.addAll(countriesData.map { it.name.common })
    }

    override fun addCountry(country: String) {
        countries.add(country)
    }

    override fun getCountries(): List<String> {
        return countries
    }
}


//class FileStorage(private val fileName: String) : Storage {
//
//    override fun addCountry(country: String) {
//        try {
//            File(fileName).appendText("$country\n")
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//    }
//
//    override fun getCountries(): List<String> {
//        return try {
//            File(fileName).readLines()
//        } catch (e: IOException) {
//            e.printStackTrace()
//            emptyList()
//        }
//    }
//}

