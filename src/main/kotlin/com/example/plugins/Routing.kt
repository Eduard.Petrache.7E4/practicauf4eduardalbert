package com.example.plugins

import com.example.routes.routesKtor
import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*

fun Application.configureRouting() {
    routing {
        routesKtor()
        static("/static") {
            resources("files")
        }
    }
}
