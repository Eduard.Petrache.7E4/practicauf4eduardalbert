package com.example.routes

import com.example.data.Country
import com.example.data.Storage
import com.example.data.UserSession
import com.example.templates.GameTemplate
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.html.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import com.example.templates.HomeTemplate
import com.example.templates.LoginTemplate
import io.ktor.server.request.*
import io.ktor.util.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.request.get
import io.ktor.client.statement.*
import io.ktor.http.ContentDisposition.Companion.File
import java.io.File


fun Route.routesKtor() {
    // Redirect to login as per default
    get("/") {
        call.respondRedirect("/login")
    }

    // Display login form
    get("/login") {
        call.respondHtmlTemplate(LoginTemplate()) {}
    }

    // Handle login form submission
    authenticate("auth-form") {
        post("/login") {
            val userName = call.principal<UserIdPrincipal>()?.name.toString()
            call.sessions.set(UserSession(name = userName, count = 1))
            call.respondRedirect("/home")
        }
    }

    // Handle home page
    authenticate("auth-session") {
        get("/home") {
            val userSession = call.principal<UserSession>()
            call.sessions.set(userSession?.copy(count = userSession.count + 1))
            call.respondHtmlTemplate(HomeTemplate(userSession?.name, userSession?.count ?: 0)) {}
        }

        get("/game") {
            val httpClient = HttpClient(Apache)
            val url = "https://restcountries.com/v3.1/all?fields=name,flags"
            val response: HttpResponse = httpClient.use { it.get(url) }
            val responseBody: String = response.bodyAsText()
            val countries = Json { ignoreUnknownKeys = true }.decodeFromString<List<Country>>(responseBody)
            val randomCountry = countries.random()

            call.respondHtmlTemplate(
                GameTemplate(
                    country = randomCountry.name.common,
                    flagUrl = randomCountry.flags.png
                )
            ) {}
        }

        post("/game") {
            val userAnswer = call.receiveParameters()["country"]
            val correctAnswer = call.parameters["country"]
            val storage = call.application.attributes[AttributeKey<Storage>("storage")]

            if (userAnswer != null && userAnswer == correctAnswer) {
                storage.addCountry(correctAnswer!!)
                call.respondText("Correct! The country was $correctAnswer.")
            } else {
                call.respondText("Incorrect. The correct answer was $correctAnswer.")
            }

            val countries = storage.getCountries()
            val json = File("countries.txt").readText()
            val countriesData = Json.decodeFromString<List<Country>>(json)
            val randomCountry = countriesData.find { it.name.common in countries }

            if (randomCountry != null) {
                call.respondHtmlTemplate(
                    GameTemplate(
                        country = randomCountry.name.common,
                        flagUrl = randomCountry.flags.png
                    )
                ) {}
            } else {
                call.respondText("Error: Could not find a random country.")
            }
        }



//        post("/game") {
//            val userAnswer = call.receiveParameters()["country"]
//            val correctAnswer = call.parameters["country"]
//            val storage = call.application.attributes[AttributeKey<Storage>("storage")]
//
//            if (userAnswer != null && userAnswer == correctAnswer) {
//                storage.addCountry(correctAnswer!!)
//                call.respondText("Correct! The country was $correctAnswer.")
//            } else {
//                call.respondText("Incorrect. The correct answer was $correctAnswer.")
//            }
//        }
    }
}

