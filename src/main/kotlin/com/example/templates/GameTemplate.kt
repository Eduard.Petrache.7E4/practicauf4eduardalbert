package com.example.templates

import io.ktor.server.html.*
import kotlinx.html.*

class GameTemplate(val country: String, val flagUrl: String) : Template<HTML> {
    override fun HTML.apply() {
        head {
            link(rel = "icon", href = "/static/logo.png", type = "image/png")
            link(rel = "stylesheet", href = "/static/style.css", type = "text/css")
        }
        body {
            div(classes = "game-container") {
                h3 { +"Guess the country of this flag:" }
                img(src = flagUrl, alt = "Flag") {
                    width = "320"
                }
                form(action = "/game?country=$country", method = FormMethod.post) {
                    div(classes = "form-group") {
                        label {
                            attributes["for"] = "country"
                            +"Country name: "
                        }
                        textInput {
                            name = "country"
                            id = "country"
                            classes = setOf("form-control")
                        }
                    }
                    div(classes = "form-group") {
                        submitInput(classes = "submit-button") {
                            value = "Submit"
                        }
                    }
                }
            }
        }
    }
}
