package com.example.templates

import io.ktor.server.html.*
import kotlinx.html.*

class HomeTemplate(private val name: String?, private val count: Int) : Template<HTML> {
    override fun HTML.apply() {
        head {
            title { +"Home" }
            link(rel = "icon", href = "/static/logo.png", type="image/png")
            link(rel = "stylesheet", href = "/static/style.css", type = "text/css")
        }
        body {
            p {
                +"Hello, $name! Visit count is $count."
            }
        }
    }
}
