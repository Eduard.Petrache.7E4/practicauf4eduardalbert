package com.example.templates

import io.ktor.server.html.*
import kotlinx.html.*

class LoginTemplate : Template<HTML> {

    override fun HTML.apply() {
        head {
            link(rel = "icon", href = "/static/logo.png", type = "image/png")
            link(rel = "stylesheet", href = "/static/style.css", type = "text/css")
        }
        body {
            div(classes = "login-container") {
                form(action = "/login", method = FormMethod.post) {
                    div(classes = "form-group") {
                        label {
                            attributes["for"] = "username"
                            +"Username: "
                        }
                        textInput {
                            name = "username"
                            id = "username"
                            classes = setOf("form-control")
                        }
                    }
                    div(classes = "form-group") {
                        label {
                            attributes["for"] = "password"
                            +"Password: "
                        }
                        passwordInput {
                            name = "password"
                            id = "password"
                            classes = setOf("form-control")
                        }
                    }
                    div(classes = "form-group") {
                        submitInput(classes = "submit-button") {
                            value = "Login"
                        }
                    }
                }
            }
        }
    }
}
